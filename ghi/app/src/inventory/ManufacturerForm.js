import React from 'react'
import { useState } from 'react';

export default function ManufacturerForm() {
    const [name, setName] = useState('');
    const [display, setDisplay] = useState('d-none');

    const handleSubmit = async (e) => {
        e.preventDefault();
        const url = 'http://localhost:8100/api/manufacturers/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify({
                name: name
            }),
            headers: { "Content-Type": "application/json" }
        };
        try {
            const res = await fetch(url, fetchConfig);
            if (res.ok) {
                setName('');
                setDisplay("");
            }
            else {
                console.log("post resp error!")
            }
        }
        catch (e) {
            console.log("Post fetch error:", e)
        }
    }

    return (
        <div className='container my-5 col-6'>
            <form onSubmit={handleSubmit}>
                <div className="form-group pb-3">
                    <label htmlFor="exampleInputEmail1">Manufacturer Name:</label>
                    <input onChange={e => setName(e.target.value)} type="text" value={name} className="form-control" placeholder="New Manufacturer Name" />
                </div>
                <button className='btn btn-info'>Add</button>
            </form>
            <h3 className={display}>{name} added!</h3>
        </div>
    )
}
