import React from 'react';
import { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
export default function Manufacturers() {
    const [manufacturers, setManufacturers] = useState([]);

    useEffect(() => {
        fetch('http://localhost:8100/api/manufacturers/')
            .then(res => res.json())
            .then(data => setManufacturers(data.manufacturers))
    }, [])

    return (
        <div className='container col-4 text-center'>
            <h1>Manufacturers:</h1>
            <Link className='btn btn-info' to="/manufacturers/new">Add Manufacturer</Link>
            <table className="table my-4">
                <thead>
                    <tr>
                        <th className="fs-4" scope="col">name</th>
                    </tr>
                </thead>
                <tbody>
                    {manufacturers.map(e => { return (<tr key={e.name}><th>{e.name}</th></tr>) })}
                </tbody>
            </table>
        </div>
    )
}
