import React from 'react'
import { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';

export default function Automobiles() {
    const [cars, setCars] = useState([]);

    useEffect(() => {
        const url = "http://localhost:8100/api/automobiles/";
        try {
            fetch(url)
                .then(resq => resq.json())
                .then(data => setCars(data.autos))
        }
        catch (e) { console.log("automobiles fetch error!") }
    }, [])


    return (
        <div className='container text-center my-3'>
            <h1>Automobiles:</h1>
            <Link className="btn btn-primary mb-3" to="/automobiles/new">Add Auto</Link>
            <table className="table">
                <thead>
                    <tr>
                        <th scope="col">VIN</th>
                        <th scope="col">Color</th>
                        <th scope="col">Year</th>
                        <th scope="col">Model</th>
                        <th scope="col">Manufacturer</th>
                    </tr>
                </thead>
                <tbody>
                    {/* map tr here */}
                    {cars.map(car => {
                        return (
                            <tr key={car.vin}>
                                <td>{car.vin}</td>
                                <td>{car.color}</td>
                                <td>{car.year}</td>
                                <td>{car.model.name}</td>
                                <td>{car.model.manufacturer.name}</td>
                            </tr>
                        )
                    })}
                </tbody>
            </table>
        </div>
    )
}
