import React from 'react';
import { Link } from 'react-router-dom';
import { useState, useEffect } from 'react';

export default function VehicleModels() {
    const [models, setModels] = useState([]);
    useEffect(() => {
        fetch("http://localhost:8100/api/models")
            .then(res => res.json())
            .then(data => {
                setModels(data.models);
            })
    }, [])


    return (
        <div>
            <h1>Vehicle Models</h1>
            <Link to="/vehiclemodels/new" className='col-2 btn btn-success'>Add 🚙</Link>
            <table className="table">
                <thead>
                    <tr>
                        <th scope="col">Name</th>
                        <th scope="col">Manufacturer</th>
                        <th scope='col'>Picture</th>
                    </tr>
                </thead>
                <tbody>
                    {models.map(e => {
                        return (
                            <tr key={e.name}>
                                <th>{e.name}</th>
                                <th>{e.manufacturer.name}</th>
                                <th>
                                    <img style={{ maxWidth: "620px" }} src={e.picture_url} />
                                </th>
                            </tr>
                        )
                    })}
                </tbody>
            </table>
        </div>
    )
}
