import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import Manufacturers from './inventory/Manufacturers';
import ManufacturerForm from './inventory/ManufacturerForm';
import Automobiles from './inventory/Automobiles';
import AutomobileForm from './inventory/AutomobileForm';
import VehicleModels from './inventory/vehicleModels';
import VehicleModelForm from './inventory/vehicleModelForm';
import SalesPersonForm from './sales/SalesPersonForm';
import PotentialCustomerForm from './sales/PotentialCustomerForm';
import SalesForm from './sales/SaleForm';
import Sales from './sales/Sales';
import SellerHistory from './sales/Seller_History';
import Technicians from './service/Technicians';
import TechForm from './service/TechForm';
import Appointments from './service/Appointments';
import AppointmentForm from './service/AppointmentForm';

function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path='/manufacturers' element={<Manufacturers />} />
          <Route path='/manufacturers/new' element={<ManufacturerForm />} />
          <Route path='/automobiles' element={<Automobiles />} />
          <Route path='/automobiles/new' element={<AutomobileForm />} />
          <Route path='/vehiclemodels' element={<VehicleModels />} />
          <Route path='/vehiclemodels/new' element={<VehicleModelForm />} />
          <Route path='/service/technicians' element={<Technicians />} />
          <Route path='/service/technicians/new' element={<TechForm />} />
          <Route path='/service/appointments' element={<Appointments />} />
          <Route path='/service/appointments/new' element={<AppointmentForm />} />
          <Route path='/salesperson/new' element={<SalesPersonForm />} />
          <Route path='/potentialcustomer/new' element={<PotentialCustomerForm />} />
          <Route path='/sales/new' element={<SalesForm />} />
          <Route path='/sales' element={<Sales />} />
          <Route path='/sellerhistory' element={<SellerHistory />} />
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
