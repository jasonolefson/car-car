import React, { useState, useEffect } from 'react';
import { Link } from "react-router-dom";

export default function AppointmentForm() {
    const [hide, setHide] = useState('d-none');
    const [show, setShow] = useState('');
    const [technicians, setTechnicians] = useState([]);
    const [customerName, setCustomerName] = useState('');
    const [reason, setReason] = useState('');
    const [vin, setVin] = useState('');
    const [date, setDate] = useState('');
    const [time, setTime] = useState('');
    const [technician, setTechnician] = useState();

    useEffect(() => {
        fetch("http://localhost:8080/api/technicians/")
            .then(res => res.json())
            .then(data => setTechnicians(data.technicians))
            .catch(e => console.log("feching techList error:", e));
    }, [])

    const clearForm = () => {
        setTechnician("");
        setCustomerName("");
        setReason("");
        setDate("");
        setTime("");
        setVin("");
        setHide("d-none");
        setShow("");
    }

    const handleSubmit = (e) => {
        e.preventDefault();
        const body = {
            customer_name: customerName,
            reason: reason,
            vin: vin,
            is_vip: false,
            status: '0',
            technician: technician,
            date: date,
            time: time
        };
        const appPostUrl = "http://localhost:8080/api/appointments/";
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(body),
            headers: { "Content-Type": "application/json" }
        };
        fetch(appPostUrl, fetchConfig)
            .then(res => res.json())
            .then(resData => { setShow("d-none"); setHide("") })
            .catch(e => console.log("post error:", e));
    }

    return (
        <div>
            <div className="container">
                <div className={`row ${show}`}>
                    <div className="mx-auto col-6">
                        <div className="shadow p-4 mt-4">
                            <h1>Make An Appointment:</h1>
                            <form onSubmit={handleSubmit}>
                                <div className="form-floating mb-3">
                                    <input onChange={e => setCustomerName(e.target.value)} value={customerName} placeholder="Customer Name" required type="text" name="customer_name" id="customer_name"
                                        className="form-control" />
                                    <label htmlFor="customer_name">Customer Name</label>
                                </div>
                                <div className="form-floating mb-3">
                                    <input onChange={e => setVin(e.target.value)} value={vin} placeholder="vin" required type="text" name="vin" id="vin"
                                        className="form-control" />
                                    <label htmlFor="vin">VIN:</label>
                                </div>
                                <div className="form-floating mb-3">
                                    <input onChange={e => setDate(e.target.value)} value={date} placeholder="date" required type="date" name="date" id="date"
                                        className="form-control" />
                                    <label htmlFor="date">Date</label>
                                </div>
                                <div className="form-floating mb-3">
                                    <input onChange={e => setTime(e.target.value)} value={time} placeholder="time" required type="time" name="time" id="time"
                                        className="form-control" />
                                    <label htmlFor="time">Time</label>
                                </div>
                                <div className="form-floating mb-3">
                                    <input onChange={e => setReason(e.target.value)} value={reason} placeholder="reason" required type="text" name="reason" id="reason"
                                        className="form-control" />
                                    <label htmlFor="city">Reason:</label>
                                </div>
                                <div className="mb-3">
                                    <select onChange={e => setTechnician(e.target.value)} value={technician} required name="technician" id="technician" className="form-select">
                                        <option value="">Choose a technician:</option>
                                        {technicians.map(tech => <option key={tech.employee_number} value={tech.employee_number}>{tech.name}</option>)}
                                    </select>
                                </div>
                                <button className="btn btn-primary">Make Appointment</button>
                            </form>
                        </div>
                    </div>
                </div>
                <div className={`mx-auto col-6 text-center ${hide}`}>
                    <div className="shadow p-4 mt-4">
                        <h2>Done!</h2>
                        <button className='btn btn-info mx-3' onClick={clearForm} >Make another appointment</button>
                        <Link className='btn btn-primary' to="/service/appointments">Back to Appointment list</Link>
                    </div>
                </div>
            </div>
        </div>
    )
}
