import React, { useState } from 'react';


export default function Appointment(props) {
    const { vin, customer_name, date, time, technician, reason, id } = props.data;
    const [status, setStatus] = useState(props.data.status);

    const handleCancel = () => {
        fetch(`http://localhost:8080/api/appointments/${id}/cancel/`, { method: "put" })
            .then(res => res.json())
            .then(data => { alert(`${data.id} is cancelled!`); setStatus('2') })
            .catch(e => console.log(e));
    }
    const handleFinish = () => {
        fetch(`http://localhost:8080/api/appointments/${id}/finish/`, { method: "put" })
            .then(res => res.json())
            .then(data => { alert(`${data.id} is finished!`); setStatus('1') })
            .catch(e => console.log(e));
    }

    // setUp status <td></td>
    let statusTD = <td></td>;
    switch (status) {
        case "1":
            statusTD = <td>finished</td>;
            break;
        case "2":
            statusTD = <td>cancelled</td>;
            break
        default:
            statusTD = <td><button onClick={handleCancel} className='btn-sm btn-danger'>cancel</button> <button onClick={handleFinish} className='btn-sm btn-info'>finish</button></td>;
    }

    // setUP vip <td></td>
    let vipTD = <td></td>;
    vipTD = props.isVip ?
        <td><button className='btn-sm btn-warning' disabled>YES</button></td>
        : <td><button className='btn-sm btn-outline-secondary' disabled>NO</button></td>;

    return (
        <tr>
            <td>{vin.toUpperCase()}</td>
            <td>{customer_name}</td>
            <td>{date}</td>
            <td>{time}</td>
            <td>{technician}</td>
            <td>{reason}</td>
            {statusTD}
            {vipTD}
        </tr>
    )

}

    //    <td><button className='btn-sm btn-outline-secondary' disabled>NO</button></td>
    //    <td><button className='btn-sm btn-warning' disabled>YES</button></td>