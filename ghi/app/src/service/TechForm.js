import React from 'react'
import { useState } from 'react'

export default function TechForm() {
    const [name, setName] = useState("");
    const [employeeNumber, setEmployeeNumber] = useState('');

    const emptyForm = () => {
        setName("");
        setEmployeeNumber("");
    }

    const handleSubmit = (e) => {
        e.preventDefault();
        const postTechUrl = "http://localhost:8080/api/technicians/";
        const data = { name: name, employee_number: employeeNumber };
        const fetchConfig = ({
            method: "post",
            body: JSON.stringify(data),
            headers: { "Content-Type": "application/json" }
        });

        fetch(postTechUrl, fetchConfig)
            .then(resp => resp.json())
            .then(respData => {
                emptyForm();
            })
            .catch(e => console.log("post error", e))
    }





    return (
        <div className='container col-6 shadow my-3 p-3'>
            <div className="fs-3">Add Technician:</div>
            <form className='my-3' onSubmit={handleSubmit}>
                <div className="input-group mb-3">
                    <div className="input-group-prepend">
                        <span className="input-group-text" id="inputGroup-sizing-default">Name:</span>
                    </div>
                    <input onChange={e => setName(e.target.value)} value={name} type="text" className="form-control" name="name" placeholder='Technician Name' aria-label="Default" aria-describedby="inputGroup-sizing-default" />
                </div>
                <div className="input-group mb-3">
                    <div className="input-group-prepend">
                        <span className="input-group-text" id="inputGroup-sizing-default">Employee Number:</span>
                    </div>
                    <input onChange={e => setEmployeeNumber(e.target.value)} value={employeeNumber} type="text" className="form-control" name="employeeNumber" placeholder='Technician Employee Number' aria-label="Default" aria-describedby="inputGroup-sizing-default" />
                </div>
                <button className='btn btn-info'>Add</button>
            </form>
        </div>
    )
}
