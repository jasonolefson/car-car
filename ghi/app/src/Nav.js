import { NavLink } from 'react-router-dom';

function Nav() {
  return (
    <nav className="navbar navbar-expand-lg navbar-dark bg-success">
      <div className="container-fluid">
        <NavLink className="navbar-brand" to="/">CarCar</NavLink>
        <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav me-auto mb-2 mb-lg-0">
            <li className="nav-item dropdown">
              <a className="nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                Inventory
              </a>
              <ul className="dropdown-menu">
                <li><NavLink className="dropdown-item" to="/manufacturers">Manufacturers</NavLink></li>
                <li><NavLink className="dropdown-item" to="/vehiclemodels">VehicleModels</NavLink></li>
                <li><NavLink className="dropdown-item" to="/automobiles">Automobiles</NavLink></li>
              </ul>
            </li>
            <li className="nav-item dropdown">
              <a className="nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                Service Dept
              </a>
              <ul className="dropdown-menu">
                <li><NavLink className="dropdown-item" to="/service/technicians">Technicians</NavLink></li>
                <li><NavLink className="dropdown-item" to="/service/appointments">Appointments</NavLink></li>
              </ul>
            </li>
            {/* sales dept start */}
            <li className="nav-item dropdown">
              <a className="nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                Sales Dept
              </a>
              <ul className="dropdown-menu">
                <li><NavLink className="dropdown-item" to="/potentialcustomer/new">Add a Potential Customer</NavLink></li>
                <li><NavLink className="dropdown-item" to="/salesperson/new">Add a SalePerson</NavLink></li>
                <li className="nav-item">
              <NavLink className="dropdown-item" to="/sales/new">Add a Sale!</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="dropdown-item" to="/sales">Past Sales</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="dropdown-item" to="/sellerhistory">Seller History</NavLink>
            </li>
                
              </ul>
            </li>
            {/* sales dept end */}
          </ul>
        </div>
      </div>
    </nav>
  )
}

export default Nav;





            // <li className="nav-item">
            //   <NavLink className="nav-link" to="/manufacturers">Manufacturers</NavLink>
            // </li>
            // <li className="nav-item">
            //   <NavLink className="nav-link" to="/vehiclemodels">VehicleModels</NavLink>
            // </li>
            // <li className="nav-item">
            //   <NavLink className="nav-link" to="/automobiles">Automobiles</NavLink>
            // </li>
            // <li className="nav-item">
            //   <NavLink className="nav-link" to="/service/technicians">Technicians</NavLink>
            // </li>
            // <li className="nav-item">
            //   <NavLink className="nav-link" to="/service/appointments">Appointments</NavLink>
            // </li>