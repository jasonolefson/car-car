from django.db import models

# Create your models here.

class AutomobileVO(models.Model):
    import_href = models.CharField(max_length=200,unique=True)
    vin = models.CharField(max_length=200)

    def __str__(self) -> str:
        return f"import_href is {self.import_href}"

class Technician(models.Model):
    name = models.CharField(max_length=200)
    employee_number = models.CharField(max_length=200,unique=True)

    def __str__(self) -> str:
        return f"{self.name}"

class ServiceAppointment(models.Model):
    customer_name = models.CharField(max_length=200)
    date = models.DateField()
    time = models.TimeField()
    reason = models.CharField(max_length=500)
    vin = models.CharField(max_length=200)
    status = models.CharField(max_length=15,choices=(('0',"submited"),('1',"finished"),('2',"cancelled")),default='0')
    # set is_vip before submite the appointment by checking vin in automobileVO
    is_vip = models.BooleanField()
    technician = models.ForeignKey("Technician",related_name="appointments", on_delete=models.PROTECT)

    def __str__(self) -> str:
        return f"{self.vin} reason: {self.reason} date:{self.date} time: {self.time}"
    
    def finish(self):
        self.status= '1'
        self.save()

    def cancel(self):
        self.status="2"
        self.save()