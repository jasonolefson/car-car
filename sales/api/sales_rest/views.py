from django.shortcuts import render
from django.http import JsonResponse, HttpResponse
from django.views.decorators.http import require_http_methods
import json
from common.json import ModelEncoder

from sales_rest.models import AutomobileVO, SalesPerson, PotentialCustomer, Sale
# Create your ENCODERS here.


class AutomobileVOEncoder(ModelEncoder):
    model = AutomobileVO
    properties = ["vin", "sold", "import_href"]

class SalesPersonEncoder(ModelEncoder):
    model = SalesPerson
    properties = ["name", "employee_no", "id"]

class PotentialCustomerEncoder(ModelEncoder):
    model = PotentialCustomer
    properties = ["name", "address", "phone_no", "id"]

class ListSaleEncoder(ModelEncoder):
    model = Sale
    properties = ["sale_price", "automobile", "customer", "sales_person"]

    encoders = {
        'automobile': AutomobileVOEncoder(),
        'sales_person': SalesPersonEncoder(),
        'customer': PotentialCustomerEncoder()
    }

# Create your VIEWS here.

#SALES PEOPLE
#Handle adding(GET) / listing(POST) a sales person
@require_http_methods(["GET", "POST"])
def api_list_sales_people(request):
    if request.method == "GET":
        sales_people = {"sales_person":SalesPerson.objects.all()}
        return JsonResponse(sales_people, encoder=SalesPersonEncoder, safe=False)
#Handle post
    else:
        content = json.loads(request.body)
        #create new salesperson in DB
        new_sales_person = SalesPerson.objects.create(**content)
        #return new sales person detail for display
        return JsonResponse(new_sales_person, encoder=SalesPersonEncoder, safe=False)

#Get VO for sales record
@require_http_methods(["GET"])
def api_list_auto_vo(request):
    if request.method == "GET":
        sold_vehicles = [sale.automobile.vin for sale in Sale.objects.all()]
        unsold_vehicles = AutomobileVO.objects.exclude(vin__in=sold_vehicles)
        vehicle = {"vehicle": unsold_vehicles}
        return JsonResponse(vehicle, encoder=AutomobileVOEncoder, safe=False)
        

#POTENTIAL CUSTOMER
#Handle adding(GET) / listing(POST) a potential customer
@require_http_methods(["GET", "POST"])
def api_list_potential_customers(request):
    if request.method == "GET":
        potential_customers = {"potential_customer":PotentialCustomer.objects.all()}
        return JsonResponse(potential_customers, encoder=PotentialCustomerEncoder, safe=False)
#Handle post
    else:
        content = json.loads(request.body)
        #create new potential customer in DB
        new_potential_customer = PotentialCustomer.objects.create(**content)
        #return new sales person detail for display
        return JsonResponse(new_potential_customer, encoder=PotentialCustomerEncoder, safe=False)



@require_http_methods(["GET","POST", "PUT"])
def api_list_sale_records(request):
    if request.method=="GET":
        sales = Sale.objects.all()
        return JsonResponse(
            {'sales': sales},
            encoder=ListSaleEncoder
        )
    else:
        content = json.loads(request.body)
    # set customer id to content
        
        customer_id = content['customer']
        customer = PotentialCustomer.objects.get(id=customer_id)
        content['customer'] = customer

        sales_person_id = content['sales_person']
        sales_person = SalesPerson.objects.get(id=sales_person_id)
        content['sales_person'] = sales_person
        try:
            automobile_href = content['automobile']
            print(automobile_href)
            automobile = AutomobileVO.objects.get(import_href=automobile_href)
            content['automobile'] = automobile
        except AutomobileVO.DoesNotExist:
            return JsonResponse({"message": "Cannot Find Automobile HREF"}, status=400)
        
        sale = Sale.objects.create(**content)
        return JsonResponse(sale, encoder=ListSaleEncoder, safe=False)