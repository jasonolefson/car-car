from django.db import models
from django.urls import reverse

# Create your models here.
class AutomobileVO(models.Model):
    import_href = models.CharField(max_length=200, unique=True)
    year = models.PositiveSmallIntegerField()
    vin = models.CharField(max_length=200)
    sold = models.BooleanField(default=False)


class SalesPerson(models.Model):
    name = models.CharField(max_length=200)
    employee_no = models.CharField(max_length=100)

    def __str__(self):
        return self.name

    def get_api_url(self):
        return reverse('api_show_sales_person', kwargs={'pk': self.pk})


class PotentialCustomer(models.Model):
    name = models.CharField(max_length=200)
    address = models.CharField(max_length=300)
    #could use PhoneNumberField with a pip install....
    phone_no = models.CharField(max_length=12)

    def __str__(self):
        return self.name

    def get_api_url(self):
        return reverse('api_show_potential_customer', kwargs={'pk': self.pk})


class Sale(models.Model):
    sale_price = models.PositiveIntegerField()

# USE PROTECT, AS SALES RECORDS ARE USUALLY KEPT FOR AN EXTENDED PERIOD OF TIME!!!!
    automobile = models.ForeignKey(
        AutomobileVO,
        related_name="salesrecord",
        on_delete=models.PROTECT
    )

    customer = models.ForeignKey(
        PotentialCustomer,
        related_name="salesrecord",
        on_delete=models.PROTECT
    )

    sales_person = models.ForeignKey(
        SalesPerson,
        related_name="salesrecord",
        on_delete=models.PROTECT,
    )