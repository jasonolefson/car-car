from django.urls import path
from .views import api_list_sales_people, api_list_potential_customers, api_list_sale_records, api_list_auto_vo


#localhost:8090/api/
urlpatterns = [
    #POST LIST all sales people:
    path("sales_people/", api_list_sales_people),
    path("potential_customers/", api_list_potential_customers),
    path("sale_records/", api_list_sale_records),
    path("sellable_cars/", api_list_auto_vo),
    ]