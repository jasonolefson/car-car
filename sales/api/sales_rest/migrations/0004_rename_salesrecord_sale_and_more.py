# Generated by Django 4.0.3 on 2022-09-14 16:10

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('sales_rest', '0003_automobilevo_sold'),
    ]

    operations = [
        migrations.RenameModel(
            old_name='SalesRecord',
            new_name='Sale',
        ),
        migrations.AlterField(
            model_name='potentialcustomer',
            name='phone_no',
            field=models.CharField(max_length=12),
        ),
    ]
